    Kamu baru saja memberikan testimonial sebagai berikut: <br>
    Kesan : {{ $testimonials->kesan }} <br>
    Pesan : {{ $testimonials->pesan }} <br>
    <br>
    Terima kasih atas partisipasinya. <br>
    <br>
    Regards, <br>
    {{ config('app.name') }}