<?php

namespace Modules\SimpleModule\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestimonialReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $testimonials;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($testimonials)
    {
        $this->testimonials = $testimonials;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.testimonialreceived.received');
    }
}
