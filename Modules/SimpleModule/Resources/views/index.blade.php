@extends('simplemodule::layouts.master')

@section('content')
    <h3>Selamat datang di User Dashboard</h3>
    
    <button class="btn btn-primary">
        <a class="text-white text-decoration-none" href="/simplemodule/pesan">Tambah pesan</a>
    </button>

    @forelse ($pesan as $ps)
    <p>
        <strong>
            <a href="/simplemodule/pesan/{{ $ps->id }}">{{ $ps->kesan }}</a>
        </strong>
        ({{ $ps->pesan }})
    </p>
    @empty
        <p>Tidak ada pesan untuk ditampilkan</p>
    @endforelse

@endsection
