@extends('simplemodule::layouts.master')

@section('content')
    <h1>Tulis Kesan dan Pesanmu</h1>
    
    <div class="container col-6">
        <form action="/simplemodule/pesan/store" method="post">
            @csrf
    
            <div class="form-group">
                <label for="pesanTextArea">Kesan</label>
                <textarea class="form-control" id="pesanTextArea" name="kesan" rows="3"></textarea>
            </div>
            @error('kesan')
                <p style="color: red">{{ $message }}</p>
            @enderror
            <div class="form-group">
                <label for="pesanTextArea">Pesan</label>
                <textarea class="form-control" id="pesanTextArea" name="pesan" rows="3"></textarea>
            </div>
            @error('pesan')
                <p style="color: red">{{ $message }}</p>
            @enderror

            <button class="btn btn-primary" type="submit">Simpan</button>
        </form>
    </div>

@endsection
